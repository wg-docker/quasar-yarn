# Set the base image for subsequent instructions
FROM node:14-alpine

# Update packages
RUN apk update

# Install needed pakages
RUN apk add git openssh-client rsync

# Install Yarn and add to PATH
RUN apk add yarn
RUN echo 'export PATH="$(yarn global bin):$PATH"' >> ~/.bashrc && source ~/.bashrc

# Clear out the local repository of retrieved package files
RUN rm -rf /var/cache/apk/*

# Install Quasar CLI
RUN yarn global add @quasar/cli
